<?

# write the .db files in /srv/bugs.qa.debian.org/data/db/
# reads:
#  removal hints
#  Sources.gz
#  bugs from LDAP dumps
#  pseudo package list

include('config.php');
include('common.php');
set_time_limit(180);


#$files=array('bin', 'binTesting', 'dist', 'maint', 'section', 'priority', 'version', 'versionSid', 'removal', 'bugs', 'kernel', 'pseudo');
$files=array('bin', 'binTesting', 'dist', 'maint', 'section', 'priority', 'versionSid', 'removal', 'bugs', 'kernel', 'pseudo', 'stable');

foreach ($files as $i) {
 $$i = dba_open(DB_DIR."/".$i.".db.n", "n", "db4") or die ("Cant write ".$i.".db.n");
}


# first read the removal suggestions
$dir = opendir(HINTS_DIR) or die ("Cant open removal hints");
while($file=readdir($dir)) {
 if (is_dir(HINTS_DIR."/$file")) continue;
 if ($file=="README" || $file=="index.html") continue;
 $in = fopen(HINTS_DIR."/$file", "r") or die("Cant open $path/$file");
 while($line = fgets($in, 8000)) {
  if (preg_match("/^remove (.*)/", $line, $x))
   foreach (explode(" ", $x[1]) as $k)
    dba_replace($k, $file, $removal);
  if (preg_match("/^finished/", $line)) break;
 }
 fclose($in);
# echo "Datei $file\n";
}
closedir($dir);


# read Sources, read Maintainer, Section, etc.
foreach (array ('testing', 'unstable') as $j) {
foreach ( array('non-free', 'contrib', 'main') as $i) {
 $fp = @popen("zcat ".FTP_DIR."/$j/$i/source/Sources.gz", "r")
 or die ("Kann Datei $j/$i nicht lesen.\n");
 while($line = fgets($fp, 8000)) {
   if (preg_match("/Package: (.*)/", $line, $x)) {
     $a=$x[1];
     dba_replace($a, $a, $bin);
     dba_replace($a, $i, $dist);
   }
   if (preg_match("/Binary: (.*)/", $line, $x))
     $b=$x[1];

   if (preg_match("/^Maintainer: (.*?) *<(.*?)>/", $line, $x))
     $c="<span title=\"".$x[2]."\"><a href=http://qa.debian.org/developer.php?login=".$x[2].">".$x[1]."</a></span>";
   #  $c=$x[1];

   if (preg_match("/^Section: (.*)/", $line, $x))
     $s=$x[1];

   if (preg_match("/^Priority: (.*)/", $line, $x))
     $p=$x[1];

   if (preg_match("/^Uploaders: /", $line))
     $u=1;

   if ((preg_match("/^$/", $line)) && ($a)) {
     if ($u==1) $c = $c." et al.";
     $u=0;
     if(!isset($p)) $p="optional";
     dba_replace($a,$c,$maint);
     dba_replace($a,$s,$section);
     dba_replace($a,$p,$priority);
     foreach ( preg_split("/,\s*/", $b) as $k)
       dba_replace($k, $a, $bin);
//       print "Binary $k\n";
     unset ($a, $c, $s, $p, $v, $b);
   }
 }
 fclose($fp);
}
}


# read Sources, read Version
foreach ( array('non-free', 'contrib', 'main') as $i) {
 $fp = @popen("zcat ".FTP_DIR."/unstable/$i/source/Sources.gz", "r")
 or die ("Kann Datei $i nicht lesen.");
 while($line = fgets($fp, 8000)) {
   if (preg_match("/Package: (.*)/", $line, $x))
     $a=$x[1];

   if (preg_match("/^Version: (.*)/", $line, $x))
     $v=$x[1];

   if ((preg_match("/^$/", $line)) && ($a)) {
     dba_replace($a,$v,$versionSid);
     unset ($a, $c, $s, $p, $v, $b);
   }
 }
 fclose($fp);
}

foreach ( array('non-free', 'contrib', 'main') as $i) {
 $fp = @popen("zcat ".FTP_DIR."/testing/$i/source/Sources.gz", "r")
 or die ("Kann Datei $i nicht lesen.");
 while($line = fgets($fp, 8000)) {
   if (preg_match("/Package: (.*)/", $line, $x)) {
     $a=$x[1];
     dba_replace($a, $a, $binTesting);
   }
   if (preg_match("/Binary: (.*)/", $line, $x))
     $b=$x[1];

   if (preg_match("/^Version: (.*)/", $line, $x))
     $v=$x[1];

   if ((preg_match("/^$/", $line)) && ($a)) {
     foreach ( preg_split("/,\s*/", $b) as $k)
       dba_replace($k, $a, $binTesting);
     # dba_replace($a,$v,$version);
     unset ($a, $c, $s, $p, $v, $b);
   }
 }
 fclose($fp);
}


# read bugs from LDAP dumps
readbugs($bugs, $binTesting, "buglist");
readbugs($kernel, $binTesting, "kernel");
readbugs($stable, "", "stable");


# read pseudo package list
$fp = @fopen(BUGS_ETC_DIR."/pseudo-packages.maintainers", "r") or die ("can't open pseudo-bugs");
while($line = fgets($fp, 8000)) {
 if (preg_match("/^([^	 ]+)[ 	]+(.*)/", $line, $x)) {
  dba_replace($x[1], $x[2], $pseudo);
  # echo "#".$x[1]."#".$x[2]."#\n";
 }
 else print "Can't parse $line";
}


# move new db files in place
foreach ($files as $i) {
 dba_close($$i);
 rename(DB_DIR."/".$i.".db.n", DB_DIR."/".$i.".db");
}

?>
