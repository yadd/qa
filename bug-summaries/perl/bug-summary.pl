#! /usr/bin/perl -w

require '/srv/bugs.debian.org/etc/config';

use DB_File;
use MLDBM qw(DB_File);
use Fcntl qw(O_RDONLY);
use CGI qw(:standard);
use POSIX qw(strftime);

my %always_ignore = map { $_ => 1 } qw(squeeze-ignore);
my %ignore     = map { $_ => 1 } qw(potato woody sarge etch lenny experimental);
my %always_quiet = map { $_ => 1 } qw(pending);
my %quiet      = map { $_ => 1 } qw(sid);
my %always     = map { $_ => 1 } qw(squeeze);
my @severities = qw(critical grave serious important normal minor wishlist);
my @rc         = qw(critical grave serious);

my $index = "$gSpoolDir/index.db";

# Build hashes mapping source packages to binary packages.
my (%source_map, %binary_map);
open SOURCE, $gPackageSource or die "$0: can't open $gPackageSource ($!)\n";
while (<SOURCE>) {
    # Derived from /srv/bugs.debian.org/cgi-bin/common.pl.
    next unless /^(\S+)\s+\S+\s+(\S.*\S)\s*$/;
    my ($pkg, $src) = ($1, $2);
    $pkg = lc $pkg;
    push @{$source_map{$src}}, $pkg;
    $binary_map{$pkg} = $src;
}
close SOURCE;	# who, me?


sub read_package_list ($)
{
    my $filename = shift;
    my @packages;
    open PACKAGES, $filename or die "$0: can't open $filename ($!)\n";
    while (<PACKAGES>) {
	next if /^#/;
	chomp;
	push @packages, $_;
    }
    close PACKAGES;
    return @packages;
}

sub binary_to_source ($) {
    my $binary = shift;
    if (exists $binary_map{$binary}) {
	return $binary_map{$binary};
    } else {
	return $binary;
    }
}

sub show_table ($@)
{
    my @packages = @_;
    my %allpackages;
    for my $package (@packages) {
	$allpackages{$package} = 1;
	$allpackages{$_} = 1 foreach @{$source_map{$package}};
    }

    my $full = (param('full') || 'no') eq 'yes';
    unless ($full) {
	my $url = url(-relative => 1);
	# This is a hack to work around an Apache bug; the full pages are
	# generated statically every 15 minutes.
	$url =~ s/\..*//;
	$url = "/$url-full.html";
	print <<EOF;
<p>This list only includes packages with RC bugs. You can also see a summary of
<a href="$url">all packages in this category</a>.</p>

EOF
    }

    my (%counts, %realcounts, %rccounts);
    my $totalrc = 0;
    my $totalrcreal = 0;

    for my $package (@packages) {
	for my $severity (@severities) {
	    $counts{$package}{$severity} = 0;
	    $realcounts{$package}{$severity} = 0;
	    if (defined $binary_map{$package}) {
		$counts{$binary_map{$package}}{$severity} = 0;
		$realcounts{$binary_map{$package}}{$severity} = 0;
	    }
	}
    }

    open INDEX, $index or die "$0: can't open $index ($!)\n";
    INDEXLINE: while (<INDEX>) {
	next unless
	  /^(\S+)\s+(\d+)\s+(\d+)\s+(\S+)\s+\[\s*([^]]*)\s*\]\s+(\w+)\s+(.*)$/;
	next unless $allpackages{$1};
	my ($pkg, $bug, $status, $submitter, $severity, $tags) =
	    ($1, $2, $4, $5, $6, $7);
	my @tags = split ' ', $tags;
	next if $status eq 'done';
	my ($always, $always_ignore, $ignore) = (0, 0);
	for my $tag (@tags) {
	    next INDEXLINE if $tag eq 'fixed';
	    $ignore ||= $ignore{$tag};
	    $always ||= $always{$tag};
	    $always_ignore ||= $always_ignore{$tag};
	}
	next INDEXLINE if $always_ignore or ($ignore and not $always);
	my $source = $binary_map{$pkg};
	$source = $pkg unless defined $source;
	++$counts{$source}{$severity};
	(my $quiet, my $always_quiet, $always) = (0, 0, 0);
	for my $tag (@tags) {
	    $quiet ||= $quiet{$tag};
	    $always_quiet ||= $always_quiet{$tag};
	    $always ||= $always{$tag};
	}
	next INDEXLINE if $always_quiet or ($quiet and not $always);
	++$realcounts{$source}{$severity};
    }
    close INDEX;

    for my $package (@packages) {
	my $rc = 0;
	my $rcreal = 0;
	for my $severity (@rc) {
	    $rc += $counts{$package}{$severity};
	    $rcreal += $realcounts{$package}{$severity};
	}
	$rccounts{$package} = $rc;
	$totalrc += $rc;
	$totalrcreal += $rcreal;
    }

    print "<p>Release-critical (RC) bugs: $totalrc";
    print " ($totalrcreal)" if $totalrcreal;
    print ".</p>\n\n";

    print qq{<table class="bugtable" summary="Table of outstanding bugs by source package and severity">\n};
    print '<tr><th></th>';
    for my $severity (@severities) {
	my $style = '';
	$style = ' class="rc"' if grep { $_ eq $severity } @rc;
	print "<th$style>$severity</th>";
    }
    print "</tr>\n";

    for my $package (sort keys %counts) {
	unless ($full) {
	    next unless $rccounts{$package};
	}
	print '<tr>';
	print qq{<td><a href="http://bugs.debian.org/src:$package">$package</a></td>};
	for my $severity (@severities) {
	    print qq{<td><a href="http://bugs.debian.org/src:$package\&amp;sev-inc=$severity\&amp;pend-exc=fixed\&amp;pend-exc=done">};
	    my $count = $counts{$package}{$severity};
	    my $real = $realcounts{$package}{$severity};
	    if ($real and grep ($_ eq $severity, @rc)) {
		print qq{<span class="rc">$count</span>};
	    } else {
		print $count;
	    }
	    if ($count and $count != $real) {
		print " ($real)";
	    }
	    print '</a></td>';
	}
	print "</tr>\n";
    }

    my $ignoredesc = join ', ', map "<em>$_</em>", sort keys %ignore;
    my $alwaysignoredesc = join ', ', map "<em>$_</em>", sort keys %always_ignore;
    my $quietdesc  = join ', ', map "<em>$_</em>", sort keys %quiet;
    my $alwaysquietdesc  = join ', ', map "<em>$_</em>", sort keys %always_quiet;
    my $alwaysdesc  = join ', ', map "<em>$_</em>", sort keys %always;

    my $mtime = strftime('%Y-%m-%d %H:%M:%S %Z', localtime((stat $index)[9]));

    print <<EOF;
</table>

<p>Bug counts in parentheses don't include bugs tagged
$alwaysquietdesc nor bugs tagged $quietdesc (except if the latter are
tagged $alwaysdesc, too).
Bugs tagged $alwaysignoredesc are ignored, bugs tagged $ignoredesc are
ignored if they aren't tagged $alwaysdesc, too.</p>

<p>If some counts don't match what bugs.debian.org says, it's either because
our database has got out of sync (in which case please mail
owner\@bugs.qa.debian.org) or because bugs.debian.org
<a href="http://bugs.debian.org/65773">doesn't display bugs against multiple
packages properly</a>.</p>

<p>Indices are mirrored from bugs.debian.org every 30 minutes.
Last update: $mtime.</p>

<p>Lists of bugs in <a href="/">other categories</a> are available.</p>

EOF
}

sub html_header ($)
{
    my $title = shift;
    print <<EOF;
Content-Type: text/html

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
 "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii">
  <title>Debian Quality Assurance Bug-Squashing -- $title</title>
  <link rel="stylesheet" href="/standard.css" type="text/css">
  <link rev="made" href="mailto:debian-qa\@lists.debian.org">
</head>

<body>

<div class="logo">
  <a href="http://www.debian.org/">
    <img src="http://www.debian.org/logos/openlogo-nd-50.png" alt="">
    <img src="http://www.debian.org/Pics/debian.jpg" alt="Debian Project">
  </a>
</div>

<h1>$title</h1>

EOF
}

sub html_footer ()
{
    print <<EOF;
<hr>
<p>Copyright (C) 2003, 2004, 2005, 2006, 2007 <a href="http://www.spi-inc.org/">SPI</a>;
see <a href="http://www.debian.org/license">license terms</a>.</p>
</body>
</html>
EOF
}

1;
