#!/bin/bash

# We don't want translated Descriptions here
LC_MESSAGES=C
export LC_MESSAGES

pkg_list=''
REASON="mia"

usage ()
  {
    echo "Usage: $0 [-b bcc-adress] [-r reason] [-h] uploader pkg1 [pkg2] ... [pkgN]"
    echo ""
    echo "    -h print this help"
    echo "    -b specify the BCC address yourself instead of generate it with mia-*@qa.debian.org"
    echo "    -c specify a X-DEBBUGS-CC address instead of using the uploader's address"
    echo "    -r give the reason of removal (mia, retired, wat, orphan)"
    echo "    uploaders - it has to be in the form of \"Name Surname <email address>\""
    echo ""
    exit 1
  }

if [ "$#" -lt 2 ] ; then
  echo "not enough paramenters" >&2
  usage
fi

while getopts "hb:r:c:" opt ; do
  case $opt in
    h) usage ;;
    b) BCCADD="$OPTARG" ;;
    c) DEBBUGSCC="$OPTARG" ;;
    r) if [ "$2" = "mia" -o "$2" = "retired" -o "$2" = "wat" -o "$2" = "orphan" ] ; then
           REASON="$OPTARG"
       else
           echo "reason '$2' not recognized" >&2
           usage
       fi;;
    *) exit 5 ;;
  esac
done
# shift away args
shift $(($OPTIND - 1))

remove () {
MAINT="$1"
TAG="`echo $MAINT | sed -e 's/.*<\(.*\)>.*/\1/' -e 's/@debian.org$//' -e 's/@/=/'`"
PKG="$2"
VERSION="`apt-cache showsrc "$PKG" | grep ^Version | cut -d' ' -f 2 | uniq | xargs`"
: ${VERSION:?"The package $PKG doesn't seem to exist"}

echo "Composing email for package $PKG $VERSION..."

if [ ! "$MAINT" ] || [ ! "$PKG" ] || [ ! "$TAG" ] ; then
  echo "Could not fetch all relevant package data for $PKG" 1>&2
  exit 1
fi

case "$REASON" in
  orphan) reason_text="$MAINT wishes no longer to be uploader of $PKG." ;;
  mia) reason_text="$MAINT has not been working on
the $PKG package for quite some time." ;;
  retired) reason_text="$MAINT has retired, so can't work on
the $PKG package anymore (at least with this address)." ;;
  wat) reason_text="The Debian account of $MAINT,
listed in the Uploaders list of $PKG, has been shut down
by the Debian Account Managers." ;;
esac


(
cat <<EOF
Source: $PKG
Version: $VERSION
Severity: minor
User: mia@qa.debian.org
Usertags: mia-teammaint

$reason_text

We are tracking their status in the MIA team and would like to ask you
to remove them from the Uploaders list of the package so we can close
that part of the file.

(If the person is listed as Maintainer, what we are asking is to please
step in as a new maintainer.)

Thanks.
EOF
) > remove-mail

if [ -z "$BCCADD" ] ; then
  BCCADD="mia-$TAG@qa.debian.org"
fi

if [ -z "$DEBBUGSCC" ] ; then
  DEBBUGSCC="$MAINT"
fi

if [ $! ] ; then
  exit 1
else
  mutt -s "Updating the $PKG Uploaders list" \
       -e "my_hdr X-Debbugs-Cc: $DEBBUGSCC" \
       -e "my_hdr X-MIA-Summary: -\\; asking for removal from $PKG Uploaders" \
       -e "set autoedit" \
       -b "$BCCADD" \
       -i remove-mail \
       submit@bugs.debian.org
fi
rm -f remove-mail
}

MAINT="$1"
shift

for PKG in "$@" ; do
	remove "$MAINT" "$PKG"
done

# vim:sw=2
