#!/usr/bin/perl

# Copyright (C) 2014-2018 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;
use warnings;
use Dpkg::Version;
use DBI;
use Encode qw(decode);
use IPC::Run qw( run timeout );
use Time::HiRes qw(time);

my $dir = "/srv/scratch/qa.debian.org/vcswatch";
my $logfile = "/srv/qa.debian.org/log/vcswatch.log";
my $stopfile = "/srv/qa.debian.org/data/vcswatch/stop";
my $verbose = (-t 1);
my $timeout = 900;
my $pkg;
my ($n_pkg, $n_new, $n_update, $n_error) = (0, 0, 0, 0);
my $starttime = time;
my $pkgstarttime;

umask 002;
$ENV{LC_ALL} = 'C.UTF-8';
delete $ENV{LANGUAGE};

my $file = '/etc/ssl/ca-global/ca-certificates.crt';
$ENV{GIT_SSL_CAINFO} = $file if -f $file;

open LOG, ">> $logfile" or die "$logfile: $!";

sub logger ($)
{
	my $message = shift;
	chomp $message;
	if ($verbose) {
		print STDERR "$message\n";
	}
	printf LOG "%s vcswatch[$$] %s\n", scalar(localtime), $message;
}

$SIG{__WARN__} = sub {
	logger ("Warning: @_");
	return 1;
};

$SIG{__DIE__} = sub {
	logger ("Error: @_");
	return 1;
};

my $dbh = DBI->connect("dbi:Pg:service=qa", 'qa', '',
	{AutoCommit => 0, RaiseError => 1, PrintError => 0});
$dbh->do("SET client_encoding = 'utf-8'");

sub error ($)
{
	my $message = shift || 'error';
	chomp $message;
	my $error = $dbh->prepare("UPDATE vcs SET last_scan = date_trunc('second', now()), next_scan = next_scan(), status = 'ERROR', error = ? WHERE package = ?");
	$error->execute($message, $pkg->{package});
	$dbh->commit;
	logger sprintf ("%s %s (%.3fs): %s",
		$pkg->{package}, $pkg->{url}, time - $pkgstarttime, $message);
	$n_error++;
	die;
}

sub trycmd (@)
{
	my ($in, $out, $err, $ok);
	eval {
		$ok = run \@_, \$in, \$out, \$err, timeout ($timeout);
		chomp $out;
	};
	unless ($ok) {
		print "@_: $err\n" if ($verbose);
		$err ||= "Error running @_" unless ($ok);
	}
	return $err;
}

sub runcmd (@)
{
	my ($in, $out, $err, $ok);
	eval {
		$ok = run \@_, \$in, \$out, \$err, timeout ($timeout);
		chomp $out;
	};
	unless ($ok) {
		$err ||= "Error running @_";
		chomp $err;
		print "@_: $err\n" if ($verbose);
		error ($err);
	}
	return decode('utf-8', $out);
}

sub process_package ($) {
	local $SIG{__DIE__}; # reset the die handler here

	my $pkg = shift;
	unless ($pkg->{vcs}) { # Vcs-Browser only
		$dbh->do("UPDATE vcs SET next_scan = NULL WHERE package = ?", undef, $pkg->{package});
		$dbh->commit;
		return;
	}

	$pkgstarttime = time;
	print "$pkg->{package}: $pkg->{vcs} $pkg->{url}: " if ($verbose);

	$pkg->{package} =~ /^((?:lib)?.)/;
	my $pkgprefix = $1;
	mkdir "$dir/$1" unless (-d "$dir/$pkgprefix");
	my $pkgdir = "$dir/$pkgprefix/$pkg->{package}";
	my $url = $pkg->{url};
	my $branch;
	my $subdirectory = ''; # path to debian/ inside repository (including trailing slash)
	my $debian_dir = 'true'; # false if debian/ directory contents are in top-level
	my ($tag, $commits, $hash, $vcslog); # "git describe" data and log since last tag
	my $warning;

	# hotfix section
	# these non-functional URLs lead to oversized checkouts:
	if ($pkg->{url} =~ m!^git://(git.donarmstrong.com|daniel-baumann.ch|live-systems.org|gitorious.org|git.ortolo.eu|git.kalgan.cc|git.complete.org|scarlet.buici.com)/!) {
		error "The git:// server on $1 is broken, try a different protocol";
	} elsif ($pkg->{url} =~ m!/DHG_packages.git!) { # https://salsa.debian.org/haskell-team/DHG_packages.git
		# use a shared directory for all Haskell packages
		$pkgdir = "$dir/shared/DHG_packages.git";
		$subdirectory = "p/$pkg->{package}/";
	} elsif ($pkg->{url} =~ m!/debcargo-conf.git!) { # https://salsa.debian.org/rust-team/debcargo-conf.git
		# use a shared directory for all Rust packages
		$pkgdir = "$dir/shared/debcargo-conf.git";
		my $p = $pkg->{package};
		$p =~ s/^rust-//; # rust-httparse -> src/httparse
		$subdirectory = "src/$p/";
	}

	# fix old alioth urls, issuing a warning
	if ($pkg->{vcs} eq 'Git') {
		$url =~ s{(https?|git)://(anonscm|git).debian.org/(git/)?}{https://anonscm.debian.org/git/};
	}
	if ($pkg->{vcs} eq 'Svn') {
		if ($url =~ s{svn://svn.debian.org/pkg-perl/trunk}{https://salsa.debian.org/perl-team/modules/packages}) {
			$pkg->{vcs} = 'Git';
		}
	}

	if ($url ne $pkg->{url}) {
		$warning = "The Vcs URL $pkg->{url} is deprecated/defunct, $url was tried instead. Please update the Vcs field in debian/control.";
		if ($pkg->{vcs} eq 'Git' and -d $pkgdir) {
			chdir $pkgdir;
			my $remote = runcmd(qw(git remote get-url origin));
			if ($remote ne $url) {
				$pkg->{valid_checkout} = 0 if ($remote ne $url);
				logger "Invalidating old checkout from $remote";
			}
			chdir '/';
		}
	}
	if (not $warning and $url =~ /anonscm.debian.org/) {
		$warning = "The Vcs URL is using anonscm.debian.org. Please update it for the move to salsa.debian.org.";
	}

	# we don't (want to) support ssh, error out early
	if ($url =~ m!ssh://!) {
		error ("SSH is not a supported protocol for public VCS URLs");
	}

	# remove old checkout after vcs/url change
	if (not $pkg->{valid_checkout} and -d $pkgdir and $pkgdir !~ m!/shared/!) {
		logger "Removing obsolete $pkgdir";
		system "rm -rf $pkgdir";
	}

	# get checkout
	if (! -d $pkgdir) {
		if ($pkg->{vcs} eq 'Bzr') {
			error ("Invalid bzr URL: $url") unless ($url =~ /^\w\S+$/);
			runcmd ('bzr', '-Ossl.cert_reqs=none', 'branch', $url, $pkgdir);
		} elsif ($pkg->{vcs} eq 'Cvs') {
			chdir "$dir/$pkgprefix";
			if ($url =~ /^(?:-d)?((?::ext:|:pserver:)\S+)$/) {
				runcmd ('cvs', '-d', $1, 'checkout', $pkg->{package});
				$branch = $pkg->{package};
			} elsif ($url =~ /^((?::ext:|:pserver:)\S+)\s+(\S+)$/) {
				runcmd ('cvs', '-d', $1, 'checkout', '-d', $pkg->{package}, $2);
				$branch = $2;
			} else {
				error ("Invalid cvs URL: $url");
			}
		} elsif ($pkg->{vcs} eq 'Darcs') {
			error ("Invalid darcs URL: $url") unless ($url =~ /^\w\S+$/);
			# darcs insists on creating tempfiles in '.'
			chdir '/tmp';
			runcmd ('darcs', 'get', $url, $pkgdir);
		} elsif ($pkg->{vcs} eq 'Git') { # Vcs-Git: url -b branch [subdirectory]
			$subdirectory = "$1/" if ($url =~ s/ \[(\w\S*)\]$//); # [subdirectory]
			($branch) = ($1) if ($url =~ s/ -b (\w\S*)$//); # -b branchname
			unless ($url =~ /^\w\S+$/) {
				error ("Invalid git URL: $url");
			}
			my $err = trycmd ('git', 'clone', '--quiet', '--bare', '--mirror', '--depth', '50', '--no-single-branch', '--template', '', $url, $pkgdir);
			if ($err) {
				if ($err =~ /dumb http transport does not support shallow capabilities/) {
					# try a full clone instead
					runcmd ('git', 'clone', '--quiet', '--bare', '--mirror', '--template', '', $url, $pkgdir);
				} else {
					error ($err);
				}
			}
		} elsif ($pkg->{vcs} eq 'Hg') {
			error ("Invalid hg URL: $url") unless ($url =~ /^\w\S+$/);
			($branch) = $url =~ /#(\S+)$/;
			runcmd ('hg', 'clone', '--noupdate', '--insecure', $url, $pkgdir);
		} elsif ($pkg->{vcs} eq 'Mtn') {
			my @cmd = ('mtn', 'clone');
			if ($url =~ m{^mtn://\S+$}) { # uri format
				push @cmd, $url;
			} elsif ($url =~ /^(\w\S+)\s+(\S+)$/) { # host branch format
				push @cmd, $1, $2;
			} else {
				error ("Invalid monotone URL: $url");
			}
			push @cmd, $pkgdir;
			runcmd (@cmd);
		} elsif ($pkg->{vcs} eq 'Svn') {
			error ("Invalid svn URL: $url") unless ($url =~ /^\w\S+$/);
			my @cmd = ('svn', '--non-interactive', '--trust-server-cert', 'checkout');
			$url .= '/' unless ($url =~ /\/$/);
			my $error;
			my @b = ('');
			unshift @b, 'trunk/' unless ($url =~ m{/(trunk|debian)/$});
			BRANCH: for my $b (@b) {
				my @deb = ('');
				unshift @deb, 'debian/' unless ($url =~ m{/debian/$});
				for my $deb (@deb) {
					$error = trycmd (@cmd, "$url$b$deb", $pkgdir);
					unless ($error) {
						$branch = $b;
						$debian_dir = ($deb eq 'debian/');
						last BRANCH;
					}
				}
			}
			error ($error) if ($error);
		} else {
			error "Unsupported VCS $pkg->{vcs}";
		}

		chdir $pkgdir or error "chdir $pkgdir: $!";
		$n_new++;

	} else { # update checkout
		chdir $pkgdir or error "chdir $pkgdir: $!";
		if ($pkg->{vcs} eq 'Bzr') {
			if (-e ".bzr/branch/lock/held/info" and
				-M ".bzr/branch/lock/held/info" >= 1) {
				runcmd ('bzr', 'break-lock', '--force'); # break hopefully-stale lock
			}
			runcmd ('bzr', '-Ossl.cert_reqs=none', 'pull', '--overwrite');
		} elsif ($pkg->{vcs} eq 'Cvs') {
			runcmd ('cvs', 'update');
		} elsif ($pkg->{vcs} eq 'Darcs') {
			if (-e "_darcs/lock" and -M "_darcs/lock" >= 1) {
				unlink "_darcs/lock"; # break hopefully-stale lock
			}
			runcmd ('darcs', 'pull', '-a');
		} elsif ($pkg->{vcs} eq 'Git') {
			runcmd ('git', 'fetch', '--prune', '--force', 'origin', '*:*');
			$subdirectory = "$1/" if ($url =~ s/ \[(\w\S*)\]$//); # [subdirectory]
			($branch) = ($1) if ($url =~ s/ -b (\w\S*)$//); # -b branchname
		} elsif ($pkg->{vcs} eq 'Hg') {
			runcmd ('hg', 'pull', '--insecure');
			($branch) = $url =~ /#(\S+)$/;
		} elsif ($pkg->{vcs} eq 'Mtn') {
			runcmd ('mtn', 'pull');
		} elsif ($pkg->{vcs} eq 'Svn') {
			if (-e ".svn/lock" and -M ".svn/lock" >= 1) {
				runcmd ('svn', 'cleanup'); # break hopefully-stale lock
			}
			runcmd ('svn', '--non-interactive', '--trust-server-cert', 'update');
		} else {
			error "Unsupported VCS $pkg->{vcs}";
		}
		$n_update++;
	}

	# note that we have a valid checkout now
	$dbh->do("UPDATE vcs SET valid_checkout = true WHERE package = ? AND NOT valid_checkout", undef, $pkg->{package});

	# fetch changelog from checkout
	my $changelog;
	if ($pkg->{vcs} eq 'Git') {
		# look for changelog in some locations and branches
		my @branch_list = qw(HEAD debian debian/master debian/sid master);
		unshift @branch_list, $branch if ($branch); # $branch = hardcoded -b in url
		my @clpaths = $pkg->{debian_dir} ? qw(debian/changelog changelog) :
			qw(changelog debian/changelog);
		LOOP: for my $br (@branch_list) {
			if ($br eq 'HEAD') {
				# Hack: there seems to be no better way to track the remote HEAD
				# "git remote set-head origin --auto" needs git 1.8.4 on the remote side, and is also picky about local branches
				# Ask remote server what HEAD points to there
				my $out = runcmd (qw(git ls-remote --symref origin));
				# ref: refs/heads/master	HEAD  <-- this line is only present on newer gits on the remote side
				# 4fb57c4b3f828283bbc836f4b9cb4ab45f87c6b7	HEAD
				# 4fb57c4b3f828283bbc836f4b9cb4ab45f87c6b7	refs/heads/master
				if ($out =~ m!^ref: refs/heads/(\w\S*)\s+HEAD!m) {
					$br = $1;
				} else {
					unless ($out =~ m!^([0-9a-f]+)\s+HEAD$!m) {
						$warning = "HEAD not found in git ls-remote --symref origin - does this repository have a valid default branch?";
						next;
					}
					unless ($out =~ m!^$1\s+refs/heads/(\w\S*)!m) {
						$warning = "Could not find branch for HEAD rev $1 in git ls-remote --symref origin";
						next;
					}
					$br = $1;
				}
			}
			for my $clpath (@clpaths) {
				eval { $changelog = runcmd ('git', 'show', "$br:$subdirectory$clpath"); };
				if ($changelog) {
					$changelog =~ s/(^ -- [^\n]*).*/$1/ms; # reduce to first entry
					$branch = $br;
					print "$branch $subdirectory$clpath " if ($verbose);
					$debian_dir = 'false' if ($clpath eq 'changelog');
					last LOOP;
				}
			}
		}
		defined ($branch) or error ("debian/changelog not found in any branch (tried " .
			join(", ", @branch_list) . ")");

		# check tags
		my $description = runcmd ('git', 'describe', '--tags', '--abbrev=40', '--long', '--always', $branch);
		if ($description =~ /^(\w\S*)-(\d+)-g([^-]+)$/) {
			($tag, $commits, $hash) = ($1, $2, $3);
		} else {
			$hash = $description; # no tags found
		}

		# retrieve git changelog since last tag
		if ($tag and $commits and $commits > 0) {
			$vcslog = runcmd ('git', 'log', '--max-count=100', "$tag..$branch", '--');
		}

	} elsif ($pkg->{vcs} eq 'Hg') {
		$changelog = runcmd ('hg', 'cat', '-r', ($branch // 'default'), "debian/changelog");
		$changelog =~ s/(^ -- [^\n]*).*/$1/ms; # reduce to first entry

	} else {
		my $fname = "debian/changelog";
		if ($pkg->{vcs} eq 'Svn') {
			$fname = "changelog";
		} elsif (not -f $fname and -f 'changelog') {
			$fname = 'changelog';
			$debian_dir = 'false';
		}
		open (F, $fname) or error ("debian/changelog missing in $pkg->{url}");
		while (<F>) {
			$changelog .= $_;
			last if /^ -- /;
		}
		close F;
	}
	defined ($changelog) or error ("Failure reading from debian/changelog");

	$changelog =~ /^(\S+) \((\S+)\) (\S+);/ or error ("Error parsing line 1 of debian/changelog");
	my ($package, $version, $distribution) = ($1, $2, $3);
	($package eq $pkg->{package}) or error ("changelog package $package does not match source $pkg->{package}");

	my $status;
	my $archversion = Dpkg::Version->new($pkg->{package_version});
	if ($archversion < $version) {
		$status = "NEW";
	} elsif ($archversion > $version) {
		$status = "OLD";
	} elsif ($distribution eq 'UNRELEASED') {
		$status = "UNREL";
	} elsif ($commits and $commits > 0 and $pkgdir !~ m!/shared/!) { # don't consider tags in shared checkouts
		$status = "COMMITS";
	} else {
		$status = "OK";
	}
	print "$package ($version) $distribution: $status\n" if ($verbose);

	my $vcsupdate = $dbh->prepare("UPDATE vcs SET last_scan = date_trunc('second', now()), next_scan = next_scan(), branch = ?, status = ?, debian_dir = ?, changelog_version = ?, changelog_distribution = ?, changelog = ?, tag = ?, commits = ?, hash = ?, vcslog = ?, error = ? WHERE package = ?");
	eval { $vcsupdate->execute($branch, $status, $debian_dir, $version, $distribution, $changelog, $tag, $commits, $hash, $vcslog, $warning, $package); };
	if ($@) {
		error ("Probably bad debian/changelog encoding: $@");
	}
	$dbh->commit;
}

# main

# for debugging, allow processing a specific package now
my $qual = "";
if (@ARGV) {
	my $poke = $dbh->prepare("UPDATE vcs SET next_scan = date_trunc('second', now()) WHERE package LIKE ? AND next_scan > now()");
	for my $p (@ARGV) {
		$poke->execute("$p");
	}
	$dbh->commit;
	$qual = " AND package LIKE '$ARGV[0]'";
}

my $next = $dbh->prepare("SELECT * FROM vcs WHERE next_scan <= now()$qual ORDER BY next_scan LIMIT 1 FOR UPDATE SKIP LOCKED");

my $pkg0 = '';
while ($next->execute, $pkg = $next->fetchrow_hashref) {
	die "current package didn't change in main loop: $pkg0" if ($pkg and $pkg->{package} eq $pkg0);
	last if (-f $stopfile); # exit gracefully
	eval {
		process_package ($pkg) if ($pkg);
	};
	$pkg0 = $pkg->{package};
	$n_pkg++;
}

logger sprintf ("Processed %d packages in %.1fs (%d new, %d updated, %d with error)",
	$n_pkg, (time - $starttime), $n_new, $n_update, $n_error);
