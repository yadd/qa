#!/bin/sh
set -e
if [ $# -ne 2 ] ; then
	echo "Wrong number of argument: ${#}!" >&2
	echo "Syntax: gengraphs.sh <directory of rrd> <directory for images>" >&2
	exit 1
fi
cd "$1"
outdir="$2"

if ! test -d "${outdir}" ; then
	echo "Directory $outdir does not exist!" >&2
	exit 1
fi

set -- "VRULE:1377340000#000000:scanner update"
colors="#F0A3FF #0075DC #993F00 #4C005C #191919 #005C31 #2BCE48 #FFCC99 #808080 #94FFB5 #8F7C00 #9DCC00 #C20088 #003380 #FFA405 #FFA8BB #426600 #FF0010 #5EF1F2 #00998F #E0FF66 #740AFF #990000 #FFFF80 #FFFF00 #FF5005 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000 #000000"
startdate=1376560000

for n in *.rrd ; do
	m="${n%.rrd}"
	rrdtool graph --units-exponent 0 --start "${startdate}" -e now "${outdir}"/"$m"_percent.png -t "$m" DEF:per="$n":percent:MAX  LINE3:per#ff0000:percentage "$@" > "${outdir}/${m}_percent.size"
	rrdtool graph --units-exponent 0 --start "${startdate}" -e now "${outdir}"/"$m"_absolute.png -t "$m" DEF:abs="$n":absolute:MAX  LINE3:abs#ff0000:"absolute count" "$@" > "${outdir}/${m}_absolute.size"
done

doall() {
	cs="$1"
	shift
	title="$1"
	shift
	what="$1"
	shift
	for n in *.rrd ; do
		m="${n%.rrd}"
		c=${cs%% *}
		cs=${cs#* }
		set -- "$@" "DEF:$m=$n:$what:MAX" "LINE3:$m$c:$m"
	done
	rrdtool graph --start "${startdate}" -e now "${outdir}"/all_"$what".png -t "$title" "$@" > "${outdir}"/all_"$what".size
}
doall "$colors" "all tags (absolute value)" "absolute" "$@"
doall "$colors" "all tags (percentage)" "percent" "$@"
