#!/usr/bin/perl -w

# Copyright (C) 2012 Bart Martens <bartm@knars.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use CGI;
use LWP::Simple;

my $q = CGI->new;

sub return_error
{
	my $message = shift;
	print $q->header( "text/plain" );
	print "$message\n";
	exit 0;
}

my $good_character_re = '[a-zA-Z0-9\.\-\+]';
my $package  = $q->param('package');

return_error( "package not specified" ) if( not defined $package );
return_error( "package value contains invalid characters" ) if( $package !~ /^$good_character_re*$/ );

foreach my $watchfile ( glob "/srv/qa.debian.org/data/watch/watchfiles/${package}_*.watch" )
{
	$watchfile =~ s%.*/%%;
	print $q->redirect( "http://anonscm.debian.org/viewvc/sepwatch/trunk/watchfiles/$watchfile?view=markup" );
	exit 0;
}

return_error( "no watchfile found for this package" );

