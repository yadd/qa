#!/usr/bin/perl -w
# vim: set noexpandtab:

# Copyright (C) 2012-2014 Bart Martens <bartm@knars.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use 5.010; # // operator
use strict;
use warnings;
use CGI;
use LWP::Simple;
use LWP::UserAgent;
use JSON;
use File::Temp qw/ tempdir /;
use Date::Format;
use Date::Parse;
use Dpkg::Version;
use threads;
use constant MAX_THREADS => 3;

my $ca_dir = '/etc/ssl/ca-global';
$ENV{PERL_LWP_SSL_CA_PATH} = $ca_dir if -d $ca_dir;

sub redefined_version_compare
{
	my $left = shift;
	my $right = shift;

	$left =~ s/%7E/~/g;
	$right =~ s/%7E/~/g;
	return 0 if( $left =~ /_/ );
	return 0 if( $right =~ /_/ );

	return Dpkg::Version::version_compare( $left, $right );
}

my $q = CGI->new;

my $project_char_re = '[a-zA-Z0-9\-]';
my $pypi_project_char_re = '[a-zA-Z0-9\-\.]';
my $npmjs_project_char_re = '[a-zA-Z0-9\-\._@/]';
my $cratesio_project_char_re = '[a-zA-Z0-9_.-]';
my $github_project_char_re = '[-_.a-zA-Z0-9]';
my $hg_repository_re = '[a-zA-Z0-9\.\-:/]+';
my $href_char_re = '[a-zA-Z0-9\-\._/]';
my $href_p_char_re = '[a-zA-Z0-9\-\._/%]';
my $file_char_re = '[a-zA-Z0-9\-\._]';
my $suffix_re = 'tgz|tbz2|txz|tar\.(?:gz|bz2|xz)|zip|jar';
my $download_param = $q->param('download') // '';

# "+" are replaced by spaces, back to original request
$download_param =~ s/ /\+/g;
my $upstream_param = $q->param('upstream') // '';
my $component  = $q->param('component') // '';
my $file_param = $q->param('file') // '';
my $type_param = $q->param('type') // '';
my $compact_param  = $q->param('compact') // '';
my $over_param = $q->param('over') // 0;

sub extract_suffix
{
	my $input = shift;
	$input =~ m%\.($suffix_re)$% or return "unknown.suffix";
	return $1;
}

sub return_error
{
	chdir( "/" ); # for removal of vcs_tempdir
	my $message = shift;
	print $q->header(
		-type	=> "text/plain",
		-status	=> "500 Internal Server Error",
	);
	print "$message\n";
	exit 0;
}

sub process_request
{
	my $parms_ref = shift;

	return_error( "no data found for given parameter value(s)" )
		if( not defined $parms_ref->{'webpages_urls_ref'} or not @{$parms_ref->{'webpages_urls_ref'}} );

	my $result = '';

	foreach my $webpage_url ( @{$parms_ref->{'webpages_urls_ref'}} )
	{
		my $ua = LWP::UserAgent->new;
		$ua->agent( "Mozilla/5.0 (X11; U; Linux i386; en-us) AppleWebKit/531.2+ (KHTML, like Gecko)" );
		my $response = $ua->get( $webpage_url );
		next if( not $response->is_success );
		my $webpage_contents = $response->decoded_content;
		next if( not defined $webpage_contents );

		if( defined $parms_ref->{'version_re'} )
		{
			while( $webpage_contents =~ /$parms_ref->{'version_re'}/sg )
			{
				my $version = $1;
				my $download_url = $parms_ref->{'download_url_template'};
				$download_url =~ s/###webpage###/$webpage_url/;
				$download_url =~ s/###project###/$parms_ref->{'project'}/;
				$download_url =~ s/###version###/$version/;
				my $file = ( defined $parms_ref->{'project'} ? $parms_ref->{'project'} : $parms_ref->{'package'} );
				$file .= "-$version.".extract_suffix( $download_url );

				if( $download_param eq $file )
				{
					print $q->redirect( $download_url );
					exit 0;
				}

				$result .= $q->li( $q->a( { -href => $q->self_url . "&download=$file" }, $file ) );
			}
		}

		if( defined $parms_ref->{'file_re_template'} )
		{
			my $file_re = $parms_ref->{'file_re_template'};
			$file_re =~ s/###webpage###/$webpage_url/;

			while( $webpage_contents =~ /$file_re/sg )
			{
				my $file = $1;
	
				if( $download_param eq $file )
				{
					my $download_url = $parms_ref->{'download_url_template'};
					$download_url =~ s/###webpage###/$webpage_url/;
					$download_url =~ s/###file###/$file/;

					print $q->redirect( $download_url );
					exit 0;
				}
	
				$result .= $q->li( $q->a( { -href => $q->self_url . "&download=$file" }, $file ) );
			}
		}

		if( defined $parms_ref->{'download_url_re'} )
		{
			while( $webpage_contents =~ /$parms_ref->{'download_url_re'}/sg )
			{
				my $download_url = $1;

				my $file = $download_url;
				$file =~ s%/download$%%; # sf
				$file =~ s%.*/%%;
				my $file_unescaped = $file;
				$file_unescaped =~ s/%([0-9A-Fa-f]{2})/chr(hex($1))/eg; # man URI::Escape

				if( $download_param eq $file_unescaped )
				{
					$download_url = $parms_ref->{'baseurl'}.$download_url
						if( defined $parms_ref->{'baseurl'} and $download_url =~ m%^/% );
					print $q->redirect( $download_url );
					exit 0;
				}

				$result .= $q->li( $q->a( { -href => $q->self_url . "&download=$file" }, $file_unescaped ) );
			}
		}

		if( defined $parms_ref->{'download_url_version_re'} )
		{
			while( $webpage_contents =~ /$parms_ref->{'download_url_version_re'}/sg )
			{
				my $download_url = $1;
				eval '$download_url =~ '.$parms_ref->{'downloadurlmangle'} if( defined $parms_ref->{'downloadurlmangle'} );
				my $version = $2;

				my $file = "$parms_ref->{'package'}-$version.".extract_suffix( $download_url );

				if( $download_param eq $file )
				{
					print $q->redirect( $download_url );
					exit 0;
				}

				$result .= $q->li( $q->a( { -href => $q->self_url . "&download=$file" }, $file ) );
			}
		}

		if( defined $parms_ref->{'download_url_file_re'} )
		{
			while( $webpage_contents =~ /$parms_ref->{'download_url_file_re'}/sg )
			{
				my $download_url = $1;
				eval '$download_url =~ '.$parms_ref->{'downloadurlmangle'} if( defined $parms_ref->{'downloadurlmangle'} );
				my $file = $2;

				if( $download_param eq $file )
				{
					print $q->redirect( $download_url );
					exit 0;
				}

				$result .= $q->li( $q->a( { -href => $q->self_url . "&download=$file" }, $file ) );
			}
		}
	}

	return_error( "requested file $download_param not available" ) if( $download_param );
	return_error( "no files found for given parameter value(s)" ) if( $result eq '' );

	print $q->header;
	print $q->start_html;
	print $q->start_ul;
	print $result;
	print $q->end_ul;
	print $q->end_html;

	exit 0;
}

# get_npmjs_grouped_versions( $main_pkg, $versions, $over )
#
# Function that builds the list of versions available for download.
#
# param $main_pkg: main node module
# param $version : hashref that contains:
#                  - an entry for each component pointing to version array
#                    (sorted by Dpkg::Version, higher first)
#                  - an entry also for main module
#                  - a '_' key that points to a hash:
#                  - one entry for each version giving npmjs tarball name
# param $over : if DD choose to group sub versions into one, he can with this
#              parameter increase first digit: if one component is removed
#              without new main upstream
#              To group version, add "&compact=1" in debian/watch url
sub get_npmjs_grouped_versions {
	my ( $main_pkg, $versions, $over ) = @_;
	my ( @digits, $str );
	$digits[0] = $over;
	unless ( $versions->{$main_pkg} and @{ $versions->{$main_pkg} } ) {
		return_error("No version for $main_pkg");
	}

	my $sub_part;
	if ($compact_param) {
		foreach my $pkg ( sort keys %$versions ) {
			next if ( $pkg eq $main_pkg or $pkg eq '_' );
			unless ( @{ $versions->{$pkg} } ) {
				return_error("No version for $pkg");
			}
			my $i = 0;
			foreach my $part ( split /[\._]/, $versions->{$pkg}->[0] ) {
				if ( $part =~ s/^(\d+)// ) {
					$digits[$i] += $1;
				}
				$part =~ s/\-//g;
				$str .= $part;
				$i++;
			}
		}
		$sub_part = join( '.', @digits );
		$sub_part = "_$str" if ($str);
	}
	else {
		$sub_part = join '+~', map {
			( /^_$/ or $_ eq $main_pkg )
			  ? ()
			  : ( $versions->{$_}->[0] || return_error("No version for $_") )
		  }
		  sort keys %$versions;
	}
	return map { "$_+~$sub_part" } @{ $versions->{$main_pkg} };
}

# Thread function used to download simultaneously npmjs metadata.
# The parameter is the nmpjs package name
sub get_npmjs_pkg_versions {
	my ($p, $webpage_url,$file_re) = @_;
	my ( $versions, $dl );
	my $ua = LWP::UserAgent->new;
	$ua->agent(
"Mozilla/5.0 (X11; U; Linux i386; en-us) AppleWebKit/531.2+ (KHTML, like Gecko)"
	);
	my $response	= $ua->get($webpage_url);
	return if ( not $response->is_success );
	my $webpage_contents = $response->decoded_content;
	next if ( not defined $webpage_contents );
	my ( @tmp, $durl );

	while ( $webpage_contents =~ /$file_re/sg ) {
		my $file = $1;
		if ( $file =~ /-(\d.*)\.(?:tgz|tar\.(?:gz|bz2|xz))/ ) {
			push @tmp, $1;
			$durl->{$1} = $file;
		}
	}
	$versions = [ sort { version_compare_part( $b, $a ) } @tmp ];
	$dl = $durl;
	return ( $versions, $dl );
}

sub process_grouped_request {
	my $parms_ref = shift;
	my $main_module = $1;
	my @pkg =
	  map { $_ =~ $parms_ref->{component_template} ? $1 : () } split /[\+ ]/,
	  $upstream_param;
	my $download_url = '###webpage###-/###file###';

	# Download different versions
	my $versions = {};
	{
		for( my $i=0; $i<@pkg; $i+=MAX_THREADS ) {
			my %thr;
			for( my $j=0; $j<MAX_THREADS; $j++ ) {
				my $p = $pkg[$i+$j];
				next unless $p;
				my $webpage_url = $parms_ref->{webpage_urls_template};
				$webpage_url =~ s/###component###/$p/g;
				my $file_re = $parms_ref->{file_re_template};
				$file_re =~ s/###webpage###/$webpage_url/;
				$thr{$p} = threads->create( { 'context' => 'list' },
					sub { return get_npmjs_pkg_versions( $p, $webpage_url, $file_re ) } );
			}
			my @err;
			foreach ( keys %thr ) {
				my @tmp = $thr{$_}->join();
				( $versions->{$_}, $versions->{_}->{$_} ) = @tmp;
				push @err, $_ unless($versions->{$_});
			}
			return_error('Unable to get remote files ('.join(', ',@err)) if(@err);
		}
	}
	if ( $versions and %$versions ) {
		my @fullversions =
			get_npmjs_grouped_versions( $main_module, $versions, $over_param );
		my $dname = join( '_',
			$main_module,
			sort map { ( $_ eq $main_module or $_ eq '_' ) ? () : $_ }
			  keys %$versions );

		my $result = '';
		foreach my $fv (@fullversions) {
			my $file = "$dname-$fv.tar.gz";
			$result .= $q->li(
				$q->a(
					{ -href => $q->self_url . "&download=$file" },
					( $component ? $component : $dname ) . "-$fv"
				)
			);
			if ( $download_param eq $file and $component ne '_' ) {
				my $pkg = $component ? $component : $main_module;
				my $display = $file;
				if ( my $version = $versions->{$pkg}->[0] ) {
					my $webpage_url = $parms_ref->{webpage_urls_template};
					$webpage_url =~ s/###component###/$pkg/g;
					my $dlu		 = $parms_ref->{download_url_template};
					$dlu =~ s/###webpage###/$webpage_url/;
					$dlu =~ s/###file###/$versions->{_}->{$pkg}->{$version}/;
					print $q->redirect($dlu);
					exit 0;
				}
			}
		}
		print $q->header;
		print $q->start_html;
		print $q->start_ul;
		print $result;
		print $q->end_ul;
		print $q->end_html;

		exit 0;
	}
}

# https://tracker.debian.org/pkg/jxrlib
# http://jxrlib.codeplex.com/
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=codeplex/jxrlib
if( $upstream_param =~ m%^codeplex/($project_char_re+)$% )
{
	my $project = $1;

	my $page_url = "http://$project.codeplex.com/";
	my $contents = LWP::Simple::get( $page_url );
	return_error( "failed to get $page_url" ) if( not defined $contents );

	$contents =~ m%href="https?://$project\.codeplex\.com/releases/view/(\d+)"%
		or return_error( "could not find view number on $page_url" );
	my $view_number = $1;

	my @webpages_urls = ();
	push @webpages_urls, "http://$project.codeplex.com/releases/view/$view_number";

	process_request( {
		'webpages_urls_ref' => \@webpages_urls,
		'download_url_file_re' => "<a [^<>]*href=\"(http://$project\\.codeplex\\.com/downloads/get/\\d+)\" [^<>]*>($file_char_re+\\.(?:$suffix_re))</a>",

	} );
}

# https://tracker.debian.org/pkg/mako
# https://pypi.python.org/pypi/Mako
# https://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=pypi/Mako
elsif( $upstream_param =~ m%^pypi/($pypi_project_char_re+)% )
{
	my $project = $1;

	my $page_url = "https://pypi.python.org/pypi/$project";
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get( $page_url );
	return_error( "failed to read $page_url : $response->status_line" ) if( not $response->is_success );
	my $contents = $response->decoded_content;
	return_error( "failed to get $page_url" ) if( not defined $contents );

	my @webpages_urls = ();
	$contents =~ m%href="(/pypi/$project/$href_char_re+)"%
		or return_error( "could not find subpage on $page_url" );
	push @webpages_urls, "https://pypi.python.org$1";

	process_request( {
		'webpages_urls_ref' => \@webpages_urls,
		'download_url_re' => " href=\"(https://pypi.python.org/packages/source/$href_char_re+/$file_char_re+\\.(?:$suffix_re))(?:#md5=\\S+)?\"",
	} );
}

# https://tracker.debian.org/pkg/dvbstreamer
# http://sourceforge.net/projects/dvbstreamer/files/
# http://127.0.0.1/~bartm/cgi-bin/fakeupstream.cgi?upstream=sf/dvbstreamer/dvbstreamer
elsif( $upstream_param =~ m%^sf/($project_char_re+)(?:/($href_p_char_re+))?$% )
{
	my $project = $1;
	my $dir = $2;
	my %breakloop;
	my $counthits = 0;

	sub get_webpage
	{
		return undef if( $counthits >= 9 );
		$counthits++;

		my $url = shift;
		my $ua = LWP::UserAgent->new;
		$ua->agent( "Mozilla/5.0 (X11; U; Linux i386; en-us) AppleWebKit/531.2+ (KHTML, like Gecko)" );
		$ua->max_redirect( 0 );
		my $response = $ua->get( $url );
		return undef if( not $response->is_success );
		return $response->decoded_content;
	}

	sub list_files_from_sf
	{
		my $url = shift;
		my @file = ();

		return @file if( defined $breakloop{$url} );
		$breakloop{$url} = 1;

		my $webpage_contents = get_webpage( "http://sourceforge.net$url" );
		return @file if( not defined $webpage_contents );

		while( $webpage_contents =~ s!href="(/projects/$project/files/$href_p_char_re+/)"!! )
		{
			my $dirlink = $1;
			next if( index( $url, $dirlink ) == 0 );
			push @file, list_files_from_sf( $dirlink );
			last if( $dirlink =~ m%/(?:$project[\-_])?\d+(?:\.\d+)*(?:-stable)?/$% );
		}

		while( $webpage_contents =~ s!href="http://sourceforge\.net(/projects/$project/files/$href_p_char_re+)/download"!! )
		{
			my $file = $1;
			$file =~ m%.*/(\S+?)$% or next;
			my $shortfile = $1;

			if( $download_param eq $shortfile )
			{
				print $q->redirect( "http://sourceforge.net$file" );
				exit 0;
			}

			push @file, $file;
		}

		return @file;
	}

	my $url = "/projects/$project/files/";
	$url .= $dir if( defined $dir );
	$url .= '/' if( $url !~ m%/$% );
	my @file = list_files_from_sf( $url );

	print $q->header;
	print $q->start_html;
	print $q->start_ul;

	foreach my $file ( @file )
	{
		$file =~ m%.*/(\S+?)$% or next;
		my $shortfile = $1;
		print $q->li( $q->a( { -href => $q->self_url . "&download=$shortfile" }, $shortfile ) );
		#print $q->li( $q->a( { -href => "http://sourceforge.net$file" }, $shortfile ) );
	}

	print $q->end_ul;
	print $q->end_html;

	exit 0;
}

# http://forge.scilab.org/index.php/p/jlatexmath/downloads/
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=scilab/jlatexmath
elsif( $upstream_param =~ m%^scilab/($project_char_re+)$% )
{
	process_request( {
		'webpages_urls_ref' => [ "http://forge.scilab.org/index.php/p/$1/downloads/" ],
		'file_re_template' => "<a href=\"$href_char_re+\">($file_char_re+\\.(?:$suffix_re))</a>",
		'download_url_template' => '###webpage###get/###file###'
	} );
}

# http://code.coreboot.org/p/seabios/downloads/
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=coreboot/seabios
elsif( $upstream_param =~ m%^coreboot/($project_char_re+)$% )
{
	process_request( {
		'webpages_urls_ref' => [ "http://code.coreboot.org/p/$1/downloads/" ],
		'file_re_template' => "<a href=\"$href_char_re+\">($file_char_re+\\.(?:$suffix_re))</a>",
		'download_url_template' => '###webpage###get/###file###'
	} );
}

# https://npmjs.org/package/tilelive
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=npmjs/tilelive
elsif( $upstream_param =~ m%^npmjs/($npmjs_project_char_re+)$% )
{
	process_request( {
		'webpages_urls_ref' => [ "https://registry.npmjs.org/$1/" ],
		'file_re_template' => "\"###webpage###-/($file_char_re+\\.(?:$suffix_re))\"",
		'download_url_template' => '###webpage###-/###file###'
	} );
}

# https://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=npmjs/mongodb+npmjs/mongodb-core+npmjs/bson
elsif ( $upstream_param =~
	m%^npmjs/($npmjs_project_char_re+)(?:[\+ ]npmjs/$npmjs_project_char_re+)+$%g
  )
{
	process_grouped_request( {
		'component_template' => qr%^npmjs/($npmjs_project_char_re+)$%,
		'webpage_urls_template' => "https://registry.npmjs.org/###component###/",
		'file_re_template' => "\"###webpage###-/($file_char_re+\\.(?:$suffix_re))\"",
		'download_url_template' => '###webpage###-/###file###'
	} );
}

# http://www.ctan.org/pkg/fragmaster
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=ctan/fragmaster
elsif( $upstream_param =~ m%^ctan/($project_char_re+)$% )
{
	my $project = $1;

	my $page_url = "https://www.ctan.org/pkg/$project";
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get( $page_url );
	return_error( "failed to get $page_url" ) if ! $response->is_success;
	my $project_page = $response->decoded_content;
	return_error( "failed to open $page_url" ) if( not defined $project_page );

	return_error( "couldn't find ctan path" )
		if( $project_page !~ m%<tr>\s*<td>Sources</td>\s*<td><a href="(/tex-archive/((?:[a-z\d\-]+(?:/[a-z\d\-]+)*)))">% );
	my $part1 = $1;
	my $part2 = $2;

	process_request( {
		'webpages_urls_ref' => [ "http://www.ctan.org/$part1" ],
		'project' => $project,
		'version_re' => '<tr><td>Ver(?:&shy;)?sion</td><td>\s*(\d+(?:\.\d+)*[b]?)\s*</td></tr>',
		'download_url_template' => "http://mirrors.ctan.org/$part2.zip"
	} );
}

# http://luaforge.net/projects/md5/
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=luaforge/md5
elsif( $upstream_param =~ m%^luaforge/($project_char_re+)$% )
{
	my $project_page = LWP::Simple::get( "http://luaforge.net/projects/$1/" );
	my @webpages_urls = ();

	while( defined $project_page and $project_page =~ m%<a href="(http://files\.luaforge\.net/releases/$href_char_re+)">%sg )
	{
		my $project_version_page = LWP::Simple::get( $1 );

		while( defined $project_version_page and $project_version_page =~ m%<a href='(http://files.luaforge.net/releases/$href_char_re+)'>%sg )
		{
			push @webpages_urls, $1;
		}
	}

	process_request( {
		'webpages_urls_ref' => \@webpages_urls,
		'download_url_re' => "<a href='(http://files.luaforge.net/releases/$href_char_re+/$file_char_re+\\.(?:$suffix_re))'>",
	} );
}

# package knights :
# http://kde-apps.org/content/show.php/Knights?content=122046
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=kdeapps/122046
# package auralquiz :
# http://qt-apps.org/content/show.php?content=139127
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=qtapps/139127
# package kde-style-polyester :
# http://KDE-Look.org/content/show.php?content=27968
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=kdelook/27968/1/2
elsif( $upstream_param =~ m%^(?:kde(?:look|apps)|qtapps)/(\d+)(?:/(\d+)(?:/(\d+))?)?$% )
{
	my $theme_number = $1;
	my $hit_from;
	my $hit_to;
	$hit_from = $2 if( defined $2 );
	$hit_to = $3 if( defined $3 );

	my $mainsite = ( $upstream_param =~ m%^qtapps% ? "qt-apps.org" :
		( $upstream_param =~ m%^kdelook% ? "KDE-Look.org" : "kde-apps.org" ) );
	my $theme_page = LWP::Simple::get( "http://$mainsite/content/show.php/?content=$theme_number" );
	my $count = 0;
	my @webpages_urls = ();

	while( defined $theme_page
	and $theme_page =~ m%<a href="/content/download\.php\?(content=\d+)(?:&|&amp;)(id=\d+)(?:&|&amp;)tan=\d+(?:(?:&|&amp;)PHPSESSID=[\da-z]+)?">%sg )
	{
		$count++;

		next if( defined $hit_from and not defined $hit_to and $count != $hit_from );
		next if( defined $hit_from and defined $hit_to and $count < $hit_from );
		next if( defined $hit_from and defined $hit_to and $count > $hit_to );

		push @webpages_urls, "http://$mainsite/content/download.php?$1&$2";
	}

	process_request( {
		'webpages_urls_ref' => \@webpages_urls,
		'download_url_re' => "<a href=\"((?:http:)?$href_char_re+/$file_char_re+\\.(?:$suffix_re))(?:\.mirrorlist)?(?:\\?PHPSESSID=[\\da-z]+)?\">Click here</a>",
		'baseurl' => "http://$mainsite",
	} );
}

# http://tracker.debian.org/pkg/mysql-connector-python
# http://dev.mysql.com/downloads/connector/python/
elsif( $upstream_param =~ m%^mysql((?:/[a-z]+)+)$% )
{
	my $dirs = $1;
	my $firstpage = LWP::Simple::get( "http://dev.mysql.com/downloads$dirs/?current_os=src" );
	my @webpages_urls = ();
	while( defined $firstpage and $firstpage =~ m%<a href="(/downloads/mirror\.php\?id=\d+)">Download</a>%sg )
	{
		my $urlpart = $1;
		push @webpages_urls, "http://dev.mysql.com$urlpart";
	}

	process_request( {
		'webpages_urls_ref' => \@webpages_urls,
		'download_url_file_re' =>
			"<a href=\"(/get/Downloads/$href_char_re+/($file_char_re+\\.(?:$suffix_re))/from/http://[a-z]+\\.mysql\\.com/)\">No thanks",
		'downloadurlmangle' => 's%^%http://dev.mysql.com%',
	} );
}

# http://tracker.debian.org/pkg/trac-httpauth
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=trac-hacks/HttpAuthPlugin
elsif( $upstream_param =~ m%^trac-hacks/($project_char_re+)$% )
{
	my $project = $1;
	my $project_lc = lc( $project );

	my $page_url = "http://trac-hacks.org/svn/$project_lc";
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get( $page_url );
	return_error( "failed to get $page_url" ) if ! $response->is_success;
	my $contents = $response->decoded_content;
	return_error( "failed to open $page_url" ) if( not defined $contents );

	my @setup_url;
	push @setup_url, "http://trac-hacks.org/svn/$project_lc/trunk/setup.py" if( $contents =~ s%href="trunk/"%% );

	my $trac_version = undef;
	while( $contents =~ s%href="(\d[\d\.\-a-z]*)/"%% )
	{
		$trac_version = $1 if( not defined $trac_version );
		$trac_version = $1 if( redefined_version_compare( $trac_version, $1 ) < 0 );
	}

	if( defined $trac_version )
	{
		push @setup_url, "https://trac-hacks.org/svn/$project_lc/$trac_version/trunk/setup.py";
		push @setup_url, "https://trac-hacks.org/svn/$project_lc/$trac_version/setup.py";
	}

	$contents = undef;
	foreach $page_url ( @setup_url )
	{
		$response = $ua->get( $page_url );
		$contents = $response->decoded_content if $response->is_success;
		last if( defined $contents );
	}
	return_error( "failed to find setup.py on http://trac-hacks.org/svn/$project_lc" ) if( not defined $contents );

	$contents =~ m%version\s*=\s*'(\d+(?:\.\d+)*)'%
		or $contents =~ m%VERSION\s*=\s*'(\d[\d\.\-a-z]*)'% # trac-batchmodify
		or $contents =~ m%__version__\s*=\s*'(\d[\d\.\-a-z]*)'% # trac-graphviz
		or return_error( "failed to find version in $page_url" );
	my $version = $1;
	$version =~ s%-trac\d+(?:\.\d+)*$%%; # trac-batchmodify

	$page_url = "http://trac-hacks.org/browser/$project_lc";
	$response = $ua->get( $page_url );
	return_error( "failed to get $page_url" ) if ! $response->is_success;
	$contents = $response->decoded_content;
	return_error( "failed to open $page_url" ) if( not defined $contents );
	$contents =~ m%<a href="/changeset/(\d+)/$project_lc">\s*Last Change</a>% or return_error( "failed to find changeset number in $page_url" );
	my $changeset = $1;

	$version .= "+r$1";
	my $file = "$project_lc-$version.zip";

	if( $download_param eq $file )
	{
		print $q->redirect( "http://trac-hacks.org/changeset/$changeset/$project_lc?old_path=/&filename=$project_lc&format=zip" );
		exit 0;
	}
	return_error( "requested file $download_param not available" ) if( $download_param );

	my $file_escaped = $file;
	$file_escaped =~ s/([\+])/sprintf("%%%02X", ord($1))/seg;

	print $q->header;
	print $q->start_html;
	print $q->start_ul;
	print $q->li( $q->a( { -href => $q->self_url . "&download=$file_escaped" }, $file ) );
	print $q->end_ul;
	print $q->end_html;

	exit( 0 );
}

# http://code.google.com/p/ocropus/ hg clone -r ocropus-0.6 http://code.google.com/p/ocropus
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=vcs/hg/google/ocropus
elsif( $upstream_param =~ m%^vcs/hg/((?:google|$hg_repository_re)/($project_char_re+))$% )
{
	my $hg_repository = $1;
	my $project = $2;

	$hg_repository =~ s%^google/%http://code.google.com/p/%;

	my $vcs_tempdir = tempdir( CLEANUP => 1 );
	chdir( $vcs_tempdir ) or return_error( "failed to chdir to $vcs_tempdir" );

	my $command = 'hg init 2> /dev/null'
		.' && hg incoming --newest-first --limit 1 --template "{latesttag}\n" '.$hg_repository.' 2> /dev/null'
		.' | grep -v "^comparing with "'
		.' | head -n 1';
	my $tag = `$command`;
	$tag =~ s/^\s*(\S*)\s*$/$1/;
	return_error( "no tags found for project $project" ) if( $tag eq '' );

	chdir( "/" ); # for removal of vcs_tempdir

	my $version = $tag;
	$version =~ s/^$project-//;
	$version =~ /\d/ or return_error( "failed to extract version from tag $tag" );

	my $file = "$project-$version.tgz";
	my $download_url = "https://$project.googlecode.com/archive/$tag.tar.gz";
	if( $download_param eq $file )
	{
		print $q->redirect( $download_url );
		exit 0;
	}
	return_error( "requested file $download_param not available" ) if( $download_param );

	print $q->header;
	print $q->start_html;
	print $q->start_ul;
	print $q->li( $q->a( { -href => $q->self_url . "&download=$file" }, $file ) );
	print $q->end_ul;
	print $q->end_html;

	exit( 0 );
}

# http://code.google.com/p/squeezelite/ git ls-remote --tags http://code.google.com/p/squeezelite
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=vcs/git/google/squeezelite
elsif( $upstream_param =~ m%^vcs/git/google/($project_char_re+)$% )
{
	my $project = $1;
	my @tags;

	my $git_repository = 'http://code.google.com/p/' . $project;

	my @command = (
		'/usr/bin/git',
		'ls-remote',
		'--tags',
		$git_repository,
	);
	open(my $git_fh, '-|', @command)
		or return_error("Can't run git ls-remote: $!");
	while (<$git_fh>) {
		chomp;

		# fc4b09163f72581dbefada2eb45f6dcffa6c76b6		refs/tags/v1.0
		# 9a57a17e0c1ac801d6f7f58b39498295521938d4		refs/tags/v1.0^{}
		next unless /^[[:xdigit:]]+\s+refs\/tags\/(.+)$/;
		next if $1 =~ /\^\{\}$/;

		push @tags, $1;
	}
	close($git_fh);

	return_error("no tags found for project $project")
		if (scalar(@tags) == 0);

	my $result = '';
	for my $tag (sort @tags) {
		my $version = $tag;
		$version =~ s/^v//;
		$version =~ s/^$project-//;
		$version =~ /^\d/ or return_error("failed to extract version from tag $tag");

		my $file = "$project-$version.tar.gz";
		my $download = "https://$project.googlecode.com/archive/$tag.tar.gz";

		$result .= $q->li($q->a({ -href => $download }, $file)) . "\n";
	}

	print $q->header;
	print $q->start_html;
	print $q->start_ul;
	print $result;
	print $q->end_ul;
	print $q->end_html;
	exit 0;
}

# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=forge.puppetlabs/puppetlabs/stdlib
elsif( $upstream_param =~ m%^forge.puppetlabs/($project_char_re+)/($project_char_re+)$% )
{
	my $user = $1;
	my $project = $2;
	my $url = "https://forgeapi.puppetlabs.com/v3/modules/$user-$project";
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get( $url );
	my $refs_page = $response->decoded_content;
	return_error( "failed to read $url : $response->status_line" ) if( not $response->is_success );
	my $json_ref = JSON::decode_json( $refs_page );
	print $q->header;
	print $q->start_html;
	print $q->start_ul;
	foreach my $release ( @{$json_ref->{"releases"}} )
	{
		my $basename = join "", $user, "-", $project, "-", $release->{version}, ".tar.gz";
		print "<li>";
		print "<a href='https://forgeapi.puppetlabs.com/v3/files/", $basename, "'>", $basename, "</a>\n";
		print "</li>";
	}
	print $q->end_ul;
	print $q->end_html;
	exit 0;
}

# https://tracker.debian.org/pkg/node-stringmap
# https://github.com/olov/stringmap/
# https://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=github_commits_package_json/olov/stringmap
#
# https://tracker.debian.org/pkg/ktikz
# https://github.com/fhackenberger/ktikz/
# https://qa.debian.org/cgi-bin/fakeupstream.cgi?file=CMakeLists.txt;type=cmake;upstream=github_commits_package_json/fhackenberger/ktikz

# https://tracker.debian.org/pkg/librecaptcha
# https://github.com/nickolas360/librecaptcha/
# https://qa.debian.org/cgi-bin/fakeupstream.cgi?file=librecaptcha/librecaptcha.py;type=__version__;upstream=github_commits_package_json/nickolas360/librecaptcha
elsif( $upstream_param =~ m%^github_commits_package_json/($github_project_char_re+/$github_project_char_re+)$% )
{
	$file_param = 'package.json' unless $file_param;
	$type_param = 'package.json' unless $type_param;
	my $project = $1;
	my $url = "https://api.github.com/repos/$project/commits";
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get( $url );
	return_error( "failed to read $url : $response->status_line" ) if( not $response->is_success );
	my $json = JSON::decode_json( $response->decoded_content );
	print $q->header;
	print $q->start_html;
	print $q->start_ul;
	foreach my $commit ( @{$json} )
	{
		my $version = '';
		my $sha1 = %$commit{sha};
		my $date = %$commit{commit}->{committer}->{date};
		(my $url_date = $date) =~ s/:/-/g;
		if( $response->is_success ){
			$url = "https://github.com/$project/raw/$sha1/$file_param";
			$response = $ua->get( $url );
			if( $response->is_success ){
				if ($type_param eq 'package.json'){
					my $package_json = JSON::decode_json( $response->decoded_content );
					$version = %{$package_json}{version} if $package_json;
				} elsif ($type_param eq '__version__'){
					($version) = $response->decoded_content =~ m{__version__\s*=\s*["']([^"']+)["']};
				} elsif ($type_param eq 'cmake'){
					($version) = $response->decoded_content =~ m{set\([a-z0-9]+_version +"([^"]+)"\)}i;
				}
			}
		}
		my $uri = URI->new("https://github.com/$project/archive/$sha1.tar.gz");
		$uri->query_form( [ version => $version, date => $date ], ';' );
		print $q->li( $q->a( { -href => $uri }, "$version $date" ) );
	}

	print $q->end_ul;
	print $q->end_html;
	exit 0;
}


# https://tracker.debian.org/pkg/cargo
# https://crates.io/crates/cargo
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=crates.io/cargo
elsif( $upstream_param =~ m%^crates\.io/($cratesio_project_char_re+)% )
{
	my $project = $1;

	my $url = "https://crates.io/api/v1/crates/$project/versions";
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get( $url );
	return_error( "failed to read $url : $response->status_line" ) if( not $response->is_success );
	my $json = JSON::decode_json( $response->decoded_content );
	print $q->header;
	print $q->start_html;
	print $q->start_ul;
	foreach my $vers ( @{$json->{"versions"}} )
	{
	next if $vers->{"yanked"};
	my $version = $vers->{"num"};
	my $dl_uri = URI->new_abs($vers->{"dl_path"}, $url);

	print $q->li( $q->a( { -href => $dl_uri }, $version ) );
	}

	print $q->end_ul;
	print $q->end_html;
	exit 0;
}

# https://bugs.debian.org/803790
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=google-fonts/ofl/pompiere
elsif( $upstream_param =~ m%^google-fonts/($project_char_re+)/($project_char_re+)$% )
{
	my $font_licence = $1;
	my $font_name = $2;

	my $github_repo_url = "https://github.com/google/fonts/tree/master/$font_licence/$font_name";
	my $raw_fontlog_url = "https://raw.githubusercontent.com/google/fonts/master/$font_licence/$font_name/FONTLOG.txt";

	my @versions_eng;

	my $ua = LWP::UserAgent->new;
	my $response = $ua->get( $raw_fontlog_url );

	return_error("failed to read $raw_fontlog_url : $response->status_line")
		if(not $response->is_success);

	my $fontlog_content = $response->decoded_content;
	foreach (split(/\n/, $fontlog_content)) {
		chomp;

		# 15 July 2011 (Author) version notes
		next unless /^([0-9]{1,2}) ([A-Za-z]+) ([0-9]{4}) /;

		push @versions_eng, "$1 $2 $3"
	}

	return_error("no versions found for font $font_licence/$font_name")
		if (scalar(@versions_eng) == 0);

	my @versions;
	for my $version_eng (@versions_eng) {
		my $date = str2time($version_eng);
		my $version = time2str("%Y%m%d", $date);

		push @versions, $version;
	}

	my $result = "\n";
	for my $version (@versions) {
		my $release = "$font_name-$version";

		$result .= $q->li( $q->a( { -href => $q->self_url . "&download=$release.zip" }, $release ) ) . "\n";
	}

	print $q->header;
	print $q->start_html;
	print $q->h1( "Known versions of " . $q->em( $font_name ));
	print $q->p( "Font available at " . $q->a( { -href => "$github_repo_url" }, $github_repo_url ) );
	print $q->start_ul;
	print $result;
	print $q->end_ul;
	print $q->end_html;

	exit 0;
}


my %upstream_info_per_package =
(
	'stopwatch' =>
	{
		'webpage' => 'http://expect.sourceforge.net/stopwatch/',
		'version_re' => '<h3>Version</h3>\s*This page describes stopwatch version (\d+(?:\.\d+)*)\.\s*<h3>',
		'download_url' => 'http://expect.sourceforge.net/stopwatch/stopwatch.tar.gz',
	},
	'morsegen' =>
	{
		'webpage' => 'http://aluigi.org/mytoolz.htm',
		'version_re' => '<a href="mytoolz/morsegen.zip" name="morsegen">Morse generator (\d+(?:\.\d+)*)</a>',
		'download_url' => 'http://aluigi.org/mytoolz/morsegen.zip',
	},
	'check-postgres' =>
	{
		'webpage' => 'http://bucardo.org/wiki/Check_postgres',
		'version_re' => '<p>The latest version, (\d+(?:\.\d+)*), can be downloaded here',
		'download_url' => 'http://bucardo.org/downloads/check_postgres.tar.gz',
	},
	'mriconvert' =>
	{
		'webpage' => 'http://lcni.uoregon.edu/~jolinda/MRIConvert/',
		'version_re' => 'The latest revision is (\d+(?:\.\d+)*), released',
		'download_url' => 'http://lcni.uoregon.edu/~jolinda/MRIConvert/mriconvert_sources.zip',
	},
	'bashburn' =>
	{
		'webpage' => 'http://bashburn.dose.se/index.php?s=downloads',
		'download_url_version_re' => '<a href="(http://bashburn.dose.se/index\.php\?s=file_download&amp;id=\d+)">BashBurn (\d+(?:\.\d+)*)</a>',
	},
	'perl-doc-html' =>
	{
		'webpage' => 'http://perldoc.perl.org/',
		'version_re' => 'To find out what\'s new in Perl (\d+(?:\.\d+)*), read the',
		'download_url' => 'http://perldoc.perl.org/perldoc-html.tar.gz',
	},
	'ssl-cert-check' =>
	{
		'webpage' => 'http://prefetch.net/code/ssl-cert-check',
		'version_re' => 'Current Version: (\d+(?:\.\d+)*)',
		'download_url' => 'http://prefetch.net/code/ssl-cert-check',
	},
	'openarena' =>
	{
		'webpage' => 'http://openarena.ws/download.php',
		'download_url_version_re' => '<a href=\'(download\.php\?list\.\d+)\'>(\d+(?:\.\d+)*)</a>',
		'downloadurlmangle' => 's%^%http://openarena.ws/%',
	},
	'wikipedia2text' =>
	{
		'webpage' => 'https://svn.256bit.org/public-svn/wikipedia2text/tags/',
		'version_re' => '<dir name="\d+(?:\.\d+)*" href="(\d+(?:\.\d+)*)/" />',
		'download_url' => 'https://svn.256bit.org/public-svn/wikipedia2text/tags/###version###/wikipedia2text',
	},
	'shogun' =>
	{
		'webpage' => 'http://www.shogun-toolbox.org/page/news/newslist',
		'download_url_version_re' => '<a href="(/new/\d+/)">\s*<font size=1>SHOGUN (\d+(?:\.\d+)*)\s*</font>',
		'downloadurlmangle' => 's%^%http://www.shogun-toolbox.org%',
	},
	'aboot' =>
	{
		'webpage' => 'http://aboot.cvs.sourceforge.net/viewvc/aboot/aboot/include/config.h?view=markup',
		'version_re' => 'ABOOT_VERSION\s*&quot;(\d[\.\da-z_]*)&quot;',
		'download_url' => 'http://aboot.cvs.sourceforge.net/viewvc/aboot/aboot/?view=tar',
	},
	'libavg' =>
	{
		'webpage' => 'https://www.libavg.de/site/projects/libavg/wiki/DownLoad',
		'download_url_file_re'
			=> '<a href="(/site/attachments/download/\d+)">(libavg-(?:\d+(?:\.\d+)*)\.(?:tgz|tbz2|txz|tar\.(?:gz|bz2|xz)|zip))</a>',
		'downloadurlmangle' => 's%^%https://www.libavg.de%',
	},
	'courierpassd' =>
	{
		'webpage' => 'http://www.arda.homeunix.net/downloads/',
		'download_url_file_re' => '<a href="(http://www\.arda\.homeunix\.net/\?ddownload=\d+)" class="download-link" title="Download File">(courierpassd-(?:\d+(?:\.\d+)*)\.(?:tgz|tbz2|txz|tar\.(?:gz|bz2|xz)|zip))</a>',
	},
	'sqlite' =>
	{
		'webpage' => 'http://www.sqlite.org/download.html',
		'file_re_template' => '>(sqlite-autoconf-\d[^<]*?(?:tgz|tbz2|txz|tar\.(?:gz|bz2|xz)))<',
		'download_url' => 'http://www.sqlite.org/2013/###file###',
	},
);

# http://expect.sourceforge.net/stopwatch/
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?package=stopwatch
# http://bashburn.dose.se/index.php?s=downloads
# http://qa.debian.org/cgi-bin/fakeupstream.cgi?package=bashburn
my $package_param = $q->param('package') // '';
if( $package_param and defined $upstream_info_per_package{$package_param} )
{
	process_request( {
		'webpages_urls_ref' => [ $upstream_info_per_package{$package_param}{"webpage"} ],
		'package' => $package_param,
		'version_re' => $upstream_info_per_package{$package_param}{"version_re"},
		'file_re_template' => $upstream_info_per_package{$package_param}{"file_re_template"},
		'download_url_template' => $upstream_info_per_package{$package_param}{"download_url"},
		'download_url_version_re' => $upstream_info_per_package{$package_param}{"download_url_version_re"},
		'download_url_file_re' => $upstream_info_per_package{$package_param}{"download_url_file_re"},
		'downloadurlmangle' => $upstream_info_per_package{$package_param}{"downloadurlmangle"},
	} );
}

return_error( "no data found for given parameter value(s)" );
