#!/usr/bin/perl -w

# Copyright (C) 2012 Bart Martens <bartm@knars.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use CGI;
use LWP::Simple;

my $q = CGI->new;

sub return_error
{
	my $message = shift;
	print $q->header( "text/plain" );
	print "$message\n";
	exit 0;
}

my $good_character_re = '[a-zA-Z0-9\.\-\+]';
my $package  = $q->param('package');

return_error( "package not specified" ) if( not defined $package );
return_error( "package value contains invalid characters" ) if( $package !~ /^$good_character_re*$/ );

my $result = '';

open INPUT, "grep \"^$package \" /srv/qa.debian.org/web/watch/uscan-errors.txt 2> /dev/null |" or return_error( "failed to open uscan-errors.txt" );
while(<INPUT>)
{
	$result .= $_;
}
close INPUT;

return_error( "package not found on the list of watchfiles with errors" ) if( $result eq '' );

print $q->header( "text/plain" );
print $result;
print "\nYou can help with fixing watch files: https://wiki.debian.org/qa.debian.org/HowToHelpWithFixingWatchFiles\n";
